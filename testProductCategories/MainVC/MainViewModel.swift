import Foundation

class MainViewModel {
    
    var categoriesData : CategoriesData?
    var checkLoad = Bindable<String>("")
    
    public func getCategoriesData(){
        guard let urlForecast = URL(string: "http://62.109.7.98/api/categories") else {return}
        var requestForecast = URLRequest(url: urlForecast)
        requestForecast.httpMethod = "GET"
        let taskForecast = URLSession.shared.dataTask(with: requestForecast) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let categoryData : CategoriesData?
                    categoryData = try JSONDecoder().decode(CategoriesData.self, from: data)
                    
                    let check = categoryData?.data[0].unit
                    
                    DispatchQueue.main.async {
                        self.categoriesData = categoryData
                        self.checkLoad.value = check ?? ""
                    }
                    print(check)
                }catch {
                    print(error)
                }
            }
        }
        taskForecast.resume()
    }
}
