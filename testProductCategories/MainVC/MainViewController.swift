import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var categoriesTableView:
        UITableView!
    
    let mainViewModel = MainViewModel()
    
    var categoryData : CategoriesData?
    var checkLoading : String? {
        didSet {
            if mainViewModel.checkLoad.value != "" {
                categoryData = mainViewModel.categoriesData
                self.categoriesTableView.reloadData()
                self.activityIndicator.stopAnimating()
                self.blur.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewModel.getCategoriesData()
        checkLoaded()
        categoriesTableView.tableFooterView = UIView()    }
    
    private func checkLoaded(){
        DispatchQueue.main.async {
            self.mainViewModel.checkLoad.bind { text in
                self.checkLoading = text
            }
        }
    }
    
    private func openProductsVC(){
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "ProductsViewController" ) as? ProductsViewController else {return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.categoryData?.data.count  else {return 1}
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as? CategoryTableViewCell else {return UITableViewCell()}
        DispatchQueue.main.async {
            let categories = self.categoryData?.data[indexPath.row]
            guard let name = categories?.name else {return}
            cell.configure(categoryName: name)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = self.categoryData?.data[indexPath.row].id else {return}
        Manager.shared.categoryID = id
        openProductsVC()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

