import Foundation

struct CategoriesData : Decodable {
    var data : [CategoryData]
}

struct CategoryData: Decodable {
    var id : Int
    var name : String
    var unit : String
    var count : Int
}
