import Foundation

struct DescriptionData: Decodable {
    var data : ProductDescription
}

struct ProductDescription: Decodable {
    var name : String
    var ccal : Int
    var date : String
}
