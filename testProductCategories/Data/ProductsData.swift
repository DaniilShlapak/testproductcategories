import Foundation

struct ProductsData: Decodable {
    var data : [DataOfProducts]
}

struct DataOfProducts: Decodable {
    var id : Int
    var name : String
}
