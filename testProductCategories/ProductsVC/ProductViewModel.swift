import Foundation

class ProductViewModel {
    
    var productsData : ProductsData?
    var checkLoad = Bindable<String>("")
    
    public func getProductsData(){
        guard let urlForecast = URL(string: "http://62.109.7.98/api/product/category/\(Manager.shared.categoryID)") else {return}
        var requestForecast = URLRequest(url: urlForecast)
        requestForecast.httpMethod = "GET"
        let taskForecast = URLSession.shared.dataTask(with: requestForecast) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let productData : ProductsData?
                    productData = try JSONDecoder().decode(ProductsData.self, from: data)
                    
                    let check = productData?.data[0].name
                    
                    DispatchQueue.main.async {
                        self.productsData = productData
                        self.checkLoad.value = check ?? ""
                    }
                }catch {
                    print(error)
                }
            }
        }
        taskForecast.resume()
    }
}
