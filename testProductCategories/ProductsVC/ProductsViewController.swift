//
//  ProductsViewController.swift
//  testProductCategories
//
//  Created by Daniil Shlapak on 19.10.21.
//

import UIKit

class ProductsViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var productsTableView: UITableView!
    
    let productViewModel = ProductViewModel()
    
    var productData : ProductsData?
    var checkLoading : String? {
        didSet {
            if productViewModel.checkLoad.value != "" {
                productData = productViewModel.productsData
                self.productsTableView.reloadData()
                self.activityIndicator.stopAnimating()
                self.blur.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLoad()
        self.productViewModel.getProductsData()
        print(Manager.shared.categoryID)
    }
    
    private func checkLoad(){
        DispatchQueue.main.async {
            self.productViewModel.checkLoad.bind { text in
                self.checkLoading = text
            }
        }
    }
    
    private func openDescriptionVC(){
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "DescriptionViewController" ) as? DescriptionViewController else {return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.productData?.data.count  else {return 1}
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTableViewCell", for: indexPath) as? ProductsTableViewCell else {return UITableViewCell()}
        DispatchQueue.main.async {
            let products = self.productData?.data[indexPath.row]
            guard let name = products?.name else {return}
            cell.configure(text: name)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = self.productData?.data[indexPath.row].id else {return}
        Manager.shared.productID = id
        openDescriptionVC()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
}
