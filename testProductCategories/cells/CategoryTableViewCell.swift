//
//  CategoryTableViewCell.swift
//  testProductCategories
//
//  Created by Daniil Shlapak on 18.10.21.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(categoryName : String){
        categoryNameLabel.text = categoryName
    }
    
    

}
