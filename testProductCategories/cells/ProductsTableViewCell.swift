//
//  ProductsTableViewCell.swift
//  testProductCategories
//
//  Created by Daniil Shlapak on 19.10.21.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var productLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(text: String){
        self.productLabel.text = text
    }
    
}
