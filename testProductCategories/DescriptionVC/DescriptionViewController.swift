//
//  DescriptionViewController.swift
//  testProductCategories
//
//  Created by Daniil Shlapak on 19.10.21.
//

import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var ccalLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    let descriptionViewModel = DescriptionViewModel()
    
    var checkLoading : String? {
        didSet {
            if descriptionViewModel.dateText.value != "" {
                self.activityIndicator.stopAnimating()
                self.blur.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.descriptionViewModel.getDescriptionData()
        self.showDescription()
        self.checkLoad()
        // Do any additional setup after loading the view.
    }
    
    private func showDescription(){
        DispatchQueue.main.async {
            self.descriptionViewModel.nameText.bind { text in
                self.productNameLabel.text = text
            }
            self.descriptionViewModel.ccalText.bind { text in
                self.ccalLabel.text = text
            }
            self.descriptionViewModel.dateText.bind { text in
                self.dateLabel.text = text
            }
        }
    }
    
    private func checkLoad(){
        DispatchQueue.main.async {
            self.descriptionViewModel.checkLoad.bind { text in
                self.checkLoading = text
            }
        }
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
