//
//  DescriptionViewModel.swift
//  testProductCategories
//
//  Created by Daniil Shlapak on 19.10.21.
//

import Foundation

class DescriptionViewModel {
    
    var nameText = Bindable<String>("")
    var ccalText = Bindable<String>("")
    var dateText = Bindable<String>("")
    var checkLoad = Bindable<String>("")
    
    public func getDescriptionData(){
        guard let urlForecast = URL(string: "http://62.109.7.98/api/product/\(Manager.shared.productID)") else {return}
        var requestForecast = URLRequest(url: urlForecast)
        requestForecast.httpMethod = "GET"
        let taskForecast = URLSession.shared.dataTask(with: requestForecast) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let descriptionData : DescriptionData?
                    descriptionData = try JSONDecoder().decode(DescriptionData.self, from: data)
                    
                    let name = descriptionData?.data.name
                    let ccal = "\(descriptionData?.data.ccal ?? 0)"
                    let date = descriptionData?.data.date
                    DispatchQueue.main.async {
                        self.nameText.value = name ?? ""
                        self.ccalText.value = ccal
                        self.dateText.value = date ?? ""
                        self.checkLoad.value = name ?? ""
                    }
                }catch {
                    print(error)
                }
            }
        }
        taskForecast.resume()
    }
    
    
    
    
}
